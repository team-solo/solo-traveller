# To GovHack 2023 Judges:

The version published at https://team-solo.gitlab.io/solo-traveller/ is the one listed on the [GovHack Hackerspace](https://hackerspace.govhack.org/projects/solo_traveller_2318).

To allow development to continue, but for judging to remani fair, we have taken the following steps:
* Allow code review of our submission by tagging commit [`govhack-submission` (`80fd4fdf3`)](https://gitlab.com/team-solo/solo-traveller/-/commit/80fd4fdf3d7812e46ff718147def9214cf78cf66).
* GitLab deployed to https://team-solo.gitlab.io/solo-traveller/ from this commit at [Aug 20, 3:53pm via pipeline #973422724](https://gitlab.com/team-solo/solo-traveller/-/pipelines/973422724).
* We have disabled the CI/CD job in [`.gitlab-ci.yml`](./.gitlab-ci.yml) to prevent accidental publishes to the submission URL until judging is complete.

This has allowed development to continue for those interested.
Of note, one important change required was to add a [`LICENSE`](./LICENSE) file to ensure the code is licensed as per the teams desires, without which further development would not be possible.

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
