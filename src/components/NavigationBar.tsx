import {
    Badge,
    Button,
    Collapse,
    Container,
    Nav,
    Navbar,
    NavbarBrand,
    NavbarToggler,
    NavItem,
    NavLink
} from "reactstrap";
import { Link } from "react-router-dom";
import {useState} from "react";

const NavigationBar = () => {

    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => {
        setIsOpen(!isOpen);
    }

    return (
        <Container>
            <Navbar light expand="md" className="navbar-style">
                {/*<NavbarToggler onClick={toggle} />*/}
                <NavbarBrand className="logo" href="/solo-traveller">
                    <img alt="logo of a map pin with a magnifying glass in the centre" src="/solo-traveller/images/logo.png" style={{height: 48}} />
                </NavbarBrand>
                <NavbarBrand  href="/solo-traveller">Solo Traveller</NavbarBrand>
                {/*<Collapse isOpen={isOpen} navbar>
                    <Nav className="ml-auto" navbar>
                        <NavItem tag={Link} to="/">Home</NavItem>
                        <NavItem tag={Link} to="/stories">Stories</NavItem>
                        <NavItem tag={Link} to="/map">Map</NavItem>
                    </Nav>
                </Collapse>*/}
            </Navbar>
        </Container>
    );
}

export default NavigationBar;