import NavigationBar from "./NavigationBar";
import {Alert, Card, CardBody, Container} from "reactstrap";
import React from "react";


interface IProps {
    message: string;
}

const ErrorPage = ({message}: IProps) => {

    return (
        <Container>
            <NavigationBar />
            <Card>
                <CardBody>
                    <Alert color="danger">
                        {message}
                    </Alert>
                </CardBody>
            </Card>
        </Container>
    )

}

export default ErrorPage;