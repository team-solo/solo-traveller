export type IStoriesDTO = {
  "stories": IStoryDTO[]
}

export type IStoryDTO =
{
  slug: string,
  location: number[],
  title: string,
  story: string,
  images: {
      "url": string,
      "description": string
  } [],
  "externalLinks": string[],
  "map": {
      "wmsUrl": string,
      "defaultZoom"?: number,
      "description": string,
  } []
}