import {
    Alert,
    Button,
    Col,
    Container,
    Input,
    InputGroup,
    InputGroupText,
    ListGroup,
    ListGroupItem,
    Row
} from "reactstrap";
import {Link} from "react-router-dom";
import NavigationBar from "../components/NavigationBar";
import React, {useEffect, useState} from "react";
import {IStoryDTO} from "../stories";
import stories from '../stories.json';
import {getDistance} from "geolib";
import {MapContainer, Marker, TileLayer, useMap} from "react-leaflet";
import {Icon, latLngBounds, LatLngTuple} from "leaflet";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const formatDistance = (distanceInMetres: number) => {
    if (distanceInMetres >= 5000) {
        return (distanceInMetres / 1000.0).toFixed(0) + 'km';
    }

    if (distanceInMetres >= 1000) {
        return (distanceInMetres / 1000.0).toFixed(1) + 'km';
    }

    return distanceInMetres + 'm';
}

export default function Stories() {

    const [ showAll, setShowAll ] = useState(false);

    const [ myLocation, setMyLocation ] = useState<{lat: number, lon: number}|null>(null);
    const [ sortedStories, setSortedStories ] = useState<IStoryDTO[]>(stories.stories);
    const [ locationStatus, setLocationStatus ] = useState<'pending' | 'failed' | 'known'>();

    const [ searchTerms, setSearchTerms ] = useState('');

    const searchTermWords = searchTerms.toLowerCase().split(/\s+/);
    const filteredStories = sortedStories.filter(story => {
        if (!searchTerms) {
            return true;
        }

        return searchTermWords.every(word =>
            story.title.toLowerCase().indexOf(word) !== -1 || story.story.toLowerCase().indexOf(word) !== -1
        );
    })
    const storiesToShow = showAll ? filteredStories : filteredStories.slice(0, 3)

    const queryMyLocation = () => {

        setLocationStatus('pending');

        navigator.geolocation.getCurrentPosition(function(position) {
            const myLocation = {lat: position.coords.latitude, lon: position.coords.longitude};
            setMyLocation(myLocation);
            setSortedStories(
                stories.stories.sort((s1: IStoryDTO, s2: IStoryDTO) => {
                    const distance1 = getDistance(myLocation, {lat: s1.location[0], lon: s1.location[1]});
                    const distance2 = getDistance(myLocation, {lat: s2.location[0], lon: s2.location[1]});
                    return distance1 - distance2;
                })
            );

            setLocationStatus('known');
        }, function(error) {
            setSortedStories(stories.stories);
            setLocationStatus('failed');
        });
    }

    useEffect(() => {
        // TODO: Periodically update location in case user is walking around.
        queryMyLocation();
    }, []);

    const onSearch = (terms: string) => {
        setSearchTerms(terms);
    }

    return (
        <Container>
            <NavigationBar />
            <Row>
                <Col sm={12} lg={12}>
                    <div style={{display: 'flex', alignItems: 'center'}}>
                        <h1 style={{flexGrow: 1}}>Stories {locationStatus === 'failed' ? '' : 'near you'}</h1>
                        <InputGroup style={{maxWidth: '200px'}}>
                            <InputGroupText>
                                <FontAwesomeIcon icon={['fas', 'search']} />
                            </InputGroupText>
                            <Input value={searchTerms} onChange={(e) => onSearch(e.target.value)} />
                        </InputGroup>
                    </div>
                </Col>
                <Col lg={6} sm={12}>
                    {/*locationStatus === 'failed' && <Button onClick={queryMyLocation}>Get my location</Button>*/}
                    {locationStatus === 'pending' && <Alert color="info">Finding your location...</Alert>}
                    <ListGroup>
                        {storiesToShow.map(story =>
                            <StoryListItem
                                key={story.slug}
                                story={story}
                                distance={myLocation == null ? 0 : getDistance(myLocation, {lat: story.location[0], lon: story.location[1]})}
                            />
                        )}
                    </ListGroup>
                    <Button onClick={() => setShowAll(!showAll)} style={{marginBottom: '1em'}}>
                        {showAll ? 'Show closest...' : 'Show more...'}
                    </Button>
                </Col>
                <Col lg={6} sm={12}>
                    <StoriesMap stories={storiesToShow} />
                </Col>
            </Row>
        </Container>
    );
}

interface IStoryListItemProps {
    story: IStoryDTO;
    distance: number;
}

const StoryListItem = ({story, distance}: IStoryListItemProps) => {

    return (
        <Link to={`/story/${story.slug}`} style={{textDecoration: 'none', marginBottom: '0.5em'}}>
            <ListGroupItem style={{position: 'relative'}}>
                <div style={{display: 'flex', alignItems: 'center'}}>
                    <h5 style={{textOverflow: 'ellipsis', flexGrow: 1}}>
                        {story.title}
                    </h5>
                    <div style={{display: 'flex', gap: '0.5em', alignItems: 'center', color: '#666'}}>
                        {story.hasOwnProperty('hasAudio') && (story as any).hasAudio ? (
                            <FontAwesomeIcon icon={["fas", "volume-up"]} />
                        ) : null}

                        {story.hasOwnProperty('youtube') ? (
                            <FontAwesomeIcon icon={["fas", "video"]} />
                        ) : null}

                        {story.map.length > 0 ? (
                            <FontAwesomeIcon icon={["fas", "map"]} />
                        ) : null}

                        {story.images.length > 0 ? (
                            <FontAwesomeIcon icon={["fas", "images"]} />
                        ) : null}

                        {story.externalLinks.length > 0 ? (
                            <FontAwesomeIcon icon={["fas", "link"]} />
                        ) : null}

                        {distance === 0 ? null : (
                            <div>
                                {formatDistance(distance)}
                            </div>)}
                    </div>
                </div>
                <div style={{color: '#666'}}>
                    {story.story.length > 100 ? `${story.story.substring(0, 150)}...` : story.story}
                </div>
            </ListGroupItem>
        </Link>
    );
}

interface IStoriesMapProps {
    stories: IStoryDTO[];
}

const StoryIcon = new Icon({
    iconUrl: '/solo-traveller/images/pin.png',
    iconAnchor: [16, 32],
    iconSize: [32, 32],
})

const StoriesMap = ({stories}: IStoriesMapProps) => {
    return (
        <div style={{height: '300px', width: '100%', margin: '0.25em', border: 'solid 1px gray', borderRadius: '0.25em', position: 'relative'}}>
            <MapContainer zoom={13} style={{height: '100%', maxWidth: '100%'}}>
                <TileLayer
                    attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                {stories.map(story =>
                    <Marker
                        position={story.location as LatLngTuple}
                        icon={StoryIcon}
                        eventHandlers={{
                            click: () => document.location = `/solo-traveller/story/${story.slug}`,
                        }}
                    />
                )}
                <CalcMapBounds stories={stories} />
            </MapContainer>
        </div>
    );
}

const CalcMapBounds = ({stories}: IStoriesMapProps) => {

    const map = useMap();

    if (stories.length > 0) {

        const sortedWestEast = stories.sort(s => s.location[0]);
        const sortedSouthNorth = stories.sort(s => s.location[1]);
        const west = sortedWestEast[0].location[0];
        const east = sortedWestEast[sortedWestEast.length - 1].location[0];
        const north = sortedSouthNorth[0].location[1];
        const south = sortedSouthNorth[sortedSouthNorth.length - 1].location[1];

        map.fitBounds(latLngBounds([west, south], [east, north]));
    }

    return null;
}