import {
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    CardText,
    CardTitle,
    Col,
    Container,
    FormGroup,
    Input,
    Label,
    Row
} from "reactstrap";
import {Link, useParams} from "react-router-dom";
import stories from "../stories.json";
import NavigationBar from "../components/NavigationBar";
import React, {useEffect, useState} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {MapContainer, TileLayer} from "react-leaflet";
import {LatLngTuple} from "leaflet";

export default function Story() {

    const params = useParams<{slug: string}>()
    const story = stories.stories.find(s => s.slug === params.slug);
    const [ play, setPlay ] = useState(false);
    const [audio, setAudio] = useState<HTMLAudioElement|null>(null);

    const [ audioUrl, setAudioUrl] = useState<string|null>(null);

    useEffect(() => {
        if (story != null) {
            const url = `/solo-traveller/tts/${story?.slug}.mp3`
            fetch(url, {method: 'HEAD'}).then((response) => {
                if (response.headers.get('Content-Type') === 'audio/mpeg') {
                    setAudioUrl(url);
                }
            }).catch(() => {
                // No audio file, so just leave audioUrl as blank...
            })
        }
    }, [story]);

    if (story == null) {
        return <p>Story {params.slug} not found</p>
    }

    //const [ showAll, setShowAll ] = useState(false);
   
    const readOutStory = () => {
        if (audioUrl == null) {
            return;
        }

        if (!play){
            const current_audio =  new Audio(audioUrl);
            current_audio.play();
            setAudio(current_audio);
            setPlay(true);
            
        } else {
            audio?.pause();
            setAudio(null);
            setPlay(false);
        }
        
        // if (!story?.story) {
        //     alert('No story to read');
        //     return;
        //   }
        
        //   const voiceId: string = 'onwK4e9ZLuTAKqWW03F9';
        //   const apiKey: string = '1310906119f1f129251a3e1507f390e3';
        //   const url: string = `https://api.elevenlabs.io/v1/text-to-speech/${voiceId}`;
        
        //   const data = {
        //     text: story.story,
        //     model_id: 'eleven_monolingual_v1',
        //     voice_settings: {
        //       stability: 0.5,
        //       similarity_boost: 0.5,
        //     },
        //   };
        
        //   const headers: Record<string, string> = {
        //     'accept': 'audio/mpeg',
        //     'xi-api-key': apiKey,
        //     'Content-Type': 'application/json',
        //   };
        
        //   axios
        //   .post(url, data, { headers, responseType: 'blob' })
        //   .then((response) => {
        //     const audioBlob = new Blob([response.data], { type: 'audio/mpeg' });
        //     const audioUrl = URL.createObjectURL(audioBlob);
      
        //     // Create an audio element to play the audio
        //     const audioElement = new Audio(audioUrl);
        //     audioElement.play();
        //   })
        //   .catch((error) => {
        //     console.error(error);
        //     alert('Failed to read the story');
        //   });
    }

    return (
        <Container>
            <NavigationBar />
            <Card>
                <CardHeader>
                    <CardTitle>
                        <div style={{display: "flex", alignItems: 'center', gap: '1em'}}>
                            <Button tag={Link} to="/stories"><FontAwesomeIcon icon={['fas', 'arrow-left']} /></Button>
                            <h2 style={{flexGrow: 1, marginBottom: 0}}>{story.title}</h2>
                            {audioUrl && <Button color="primary" onClick={readOutStory}>
                                <FontAwesomeIcon icon={["fas", "volume-up"]} /> {play ? 'Pause' : 'Listen'}
                            </Button>}
                        </div>
                    </CardTitle>
                </CardHeader>

                <CardBody>

                    <Row>
                        <Col lg={4} md={6} sm={12}>
                            <CardText className="shadow-box with-text">
                                {story.story.split('\n').map(p => <p>{p}</p>)}
                            </CardText>
                        </Col>

                        {story.hasOwnProperty('youtube') &&
                            <Col lg={4} md={6} sm={12}>
                                <iframe
                                    className="shadow-box"
                                    width="100%"
                                    height="315"
                                    src={`https://www.youtube-nocookie.com/embed/${(story as any).youtube}`}
                                    title="YouTube video player"
                                    frameBorder="0"
                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                                    allowFullScreen>
                                </iframe>
                            </Col>}

                        {story.images.length > 0 ? (
                            story.images.map((img, i) =>
                                <Col lg={4} md={6} sm={12}>
                                    <StoryImage slug={story.slug} index={i} src={img.url} description={img.description} />
                                </Col>
                            )
                        ) : null}

                        {story.map.length > 0 ? (
                            <Col lg={4} md={6} sm={12}>
                                <CardText>
                                    <StoryMap
                                        defaultZoom={(story.map[0] as any).defaultZoom}
                                        location={story.location as LatLngTuple}
                                        tilesUrl={story.map[0].wmsUrl}
                                        attribution={story.map[0].description}
                                    />
                                </CardText>
                            </Col>
                        ) : null}

                        {story.externalLinks.length > 0 ? (
                            <Col lg={4} md={6} sm={12}>
                                <div className="shadow-box with-text">
                                    <CardTitle style={{marginTop: '1em'}}>
                                        <h4>Read more</h4>
                                    </CardTitle>
                                    <CardText>
                                        <ul>
                                            {story.externalLinks.map(m => <li><a href={m}>{m}</a></li>)}
                                        </ul>
                                    </CardText>
                                </div>
                            </Col>
                        ) : null}

                    </Row>

                </CardBody>
            </Card>

        </Container>
    );
}

interface IStoryImageProps {
    slug: string;
    src: string;
    description: string;
    index: number;
}

const StoryImage = ({slug, src, description, index}: IStoryImageProps) => {
    return (
        <Link to={`/story/${slug}/image/${index}`} className="shadow-box">
            <img alt={description} style={{width: '100%', border: 'solid 1px gray', margin: '0.25em', borderRadius: '0.25em', maxWidth: '100%'}} src={src} />
        </Link>
    );
}

interface IStoryMapProps {
    location: LatLngTuple
    tilesUrl: string
    attribution: string
    defaultZoom?: number
}

const StoryMap = ({location, tilesUrl, attribution, defaultZoom}: IStoryMapProps) => {

    const [ showMap, setShowMap ] = useState(true);

    return (
        <div style={{height: '300px', maxWidth: '100%', margin: '0.25em', border: 'solid 1px gray', borderRadius: '0.25em', position: 'relative'}}>
            <MapContainer center={location} zoom={defaultZoom ?? 15} style={{height: '100%', maxWidth: '100%'}}>
                <TileLayer
                    attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                <TileLayer
                    url={tilesUrl}
                    opacity={showMap ? 0.8 : 0}
                />
            </MapContainer>
            <FormGroup switch style={{position: 'absolute', top: '1em', right: '1em', zIndex: 1000, background: 'white', padding: '0.1em 0.5em 0.1em 3em', border: 'solid 1px lightgray', borderRadius: '0.25em'}}>
                <Input
                    type="switch"
                    checked={showMap}
                    onClick={() => {
                        setShowMap(!showMap);
                    }}
                />
                <Label check style={{color: 'grey'}}>Overlay map</Label>
            </FormGroup>
        </div>
    );
}