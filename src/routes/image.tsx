import {Alert, Button, Card, CardBody, CardFooter, CardHeader, CardImg, CardTitle, Container} from "reactstrap";
import {Link, useParams} from "react-router-dom";
import stories from "../stories.json";
import NavigationBar from "../components/NavigationBar";
import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import ErrorPage from "../components/ErrorPage";

export default function Image() {

    const params = useParams<{slug: string, index: string}>()
    const story = stories.stories.find(s => s.slug === params.slug);

    if (story == null) {
        return <ErrorPage message={`Story ${params.slug} not found`} />
    }

    const imageIndex = params.index ? parseInt(params.index) : 0;

    if (imageIndex > story.images.length) {
        return <ErrorPage message="Image not found" />
    }

    const url = story.images[imageIndex].url
    const description = story.images[imageIndex].description

    let nextImageUrl = null;
    let prevImageUrl = null;

    if (story.images.length > 1) {
        const nextIndex = (imageIndex + 1) % story.images.length;
        const prevIndex = (imageIndex + 1) % story.images.length;
        nextImageUrl = `/story/${story.slug}/image/${nextIndex}`
        prevImageUrl = `/story/${story.slug}/image/${prevIndex}`
    }

    return (
        <Container>
            <NavigationBar />
            <Card>
                <CardHeader>
                    <CardTitle>
                        <div style={{display: "flex", alignItems: 'center', gap: '1em'}}>
                            <Button tag={Link} to={`/story/${story.slug}`}><FontAwesomeIcon icon={['fas', 'arrow-left']} /></Button>
                            <h2 style={{flexGrow: 1, marginBottom: 0}}>{story.title}</h2>
                        </div>
                    </CardTitle>
                </CardHeader>

                <CardBody style={{position: 'relative'}}>

                    <CardImg src={url} alt={description}/>
                    
                    {prevImageUrl && <Link style={{padding: '1em', display: 'block', color: 'white', textShadow: '#000 1px 0 10px', fontSize: '5em', position: 'absolute', top: 0, bottom: 0, left: 0, width: '50%', textAlign: 'left'}} to={prevImageUrl}><FontAwesomeIcon icon={['fas', 'chevron-left']} /></Link>}
                    {nextImageUrl && <Link style={{padding: '1em', display: 'block', color: 'white', textShadow: '#000 1px 0 10px', fontSize: '5em', position: 'absolute', top: 0, bottom: 0, right: 0, width: '50%', textAlign: 'right'}} to={nextImageUrl}><FontAwesomeIcon icon={['fas', 'chevron-right']} /></Link>}

                </CardBody>

                <CardFooter>
                    <Button tag={Link} to={`/story/${story.slug}`}>Back to {story.title}</Button>
                </CardFooter>
            </Card>

        </Container>
    );
}