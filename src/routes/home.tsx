import {Button, Card, CardBody, CardHeader, Col, Container, Row} from "reactstrap";
import {Link} from "react-router-dom";
import React, { useCallback, useEffect } from "react";
import stories from '../stories.json';
import {IStoryDTO} from "../stories";
const highlightedStories = [
    "",
    "",
];

export default function Home() {

    const storiesToShow = stories.stories.filter(s => stories.highlights.indexOf(s.slug) !== -1);
    const basicMasonry = useCallback(() => {
        let numCols = 2;
        const container = document.querySelector('#masonry-with-columns') as HTMLElement;
        if (window.innerWidth < 576){
            numCols = 1;
            container.style.maxHeight = '10000000px'
        }
        const colHeights = Array(numCols).fill(0);
        Array.from(container.children).forEach((child, i) => {
            const order = i % numCols;
            (child as HTMLElement).style.order = `${order}`;
            colHeights[order] += parseFloat(String((child as HTMLElement).clientHeight));
        });
    }, []);

    useEffect(() => {
        basicMasonry();
    }, [basicMasonry]);

    return (
        <Container>
            <Row>
                <Col style={{textAlign: "center"}}>
                    <img style={{width: 128}} src="/solo-traveller/images/logo.png" alt="solo traveller logo" />
                    <h1>Solo Traveller</h1>
                </Col>
            </Row>
            <Row className="homeRow" xs="1" md="2">
                <Col style={{textAlign: "center"}}>
                    <Card>
                        <CardHeader>
                            <h2>Local stories</h2>
                        </CardHeader>
                        <CardBody>
                            <p>Find long lost historical stories, and discover significant heritage from your local area.</p>
                            <Button tag={Link} size="lg" color="primary" to="/stories">
                                Find stories near me
                            </Button>
                        </CardBody>
                    </Card>
                </Col>
                <Col style={{textAlign: "center"}}>
                    <Card>
                        <CardHeader>
                            <h2>Featured stories</h2>
                        </CardHeader>
                        <CardBody>
                            <Row xs="1" md="2" className="masonry-with-columns" id='masonry-with-columns'>
                                {storiesToShow.map(s =>
                                    <Col>
                                        <FeaturedStory story={s} />
                                    </Col>
                                )}
                            </Row>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}

interface IFeatureStoryProps {
    story: IStoryDTO
}

const FeaturedStory = ({story}: IFeatureStoryProps) =>
    <Link to={`/story/${story.slug}`} className="feature-story" style={{
    }}>
        <h4>{story.title}</h4>
        {story.images.length > 0 ? (
            <img style={{maxWidth: '100%'}} src={story.images[0].url} alt={story.images[0].description} />
        ) : null}
    </Link>
