import React from 'react';

import { library } from '@fortawesome/fontawesome-svg-core'
import {faLink, faMap, fas, faSearch} from '@fortawesome/free-solid-svg-icons'
import { faVideo, faVolumeUp, faArrowLeft, faChevronLeft, faChevronRight, faImages } from '@fortawesome/free-solid-svg-icons'
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'leaflet/dist/leaflet.css';

import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";

import Stories from "./routes/stories";
import Story from "./routes/story";
import Image from "./routes/image";
import Home from "./routes/home";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />,
  },
  {
    path: "/stories",
    element: <Stories />,
  },
  {
    path: "/story/:slug",
    element: <Story />,
  },
  {
    path: "/story/:slug/image/:index",
    element: <Image />,
  },
], {
  basename: '/solo-traveller'
});

library.add(fas, faVolumeUp)
library.add(fas, faArrowLeft)
library.add(fas, faChevronLeft)
library.add(fas, faChevronRight)
library.add(fas, faImages)
library.add(fas, faVideo)
library.add(fas, faLink)
library.add(fas, faMap)
library.add(fas, faSearch)

function App() {
  return (
    <React.StrictMode>
      <RouterProvider router={router} />
    </React.StrictMode>
  );
}

export default App;
