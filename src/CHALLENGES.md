# Challenges Attempted

1. Discover Heritage: Unravel your local stories
   https://hackerspace.govhack.org/challenges/discover_heritage_unravel_your_local_stories

    Heritage holds the untold stories of our cultural roots, helping to shape our identities and give us a sense of wellbeing and belonging to a wider community. We aim to ignite a passion for heritage, bridging the gap between people and their past. We want inventive ways that we can empower communities to discover and preserve heritage and their local narratives.

    Understanding heritage's universal value is key. By celebrating its role in enriching lives, we forge a profound connection with our collective past. Preserving traditions is essential, and collaborative efforts, education, and intergenerational exchanges keep these legacies alive.

    Heritage strengthens community bonds, fostering stewardship. Through cultural celebrations and inclusive initiatives, we create spaces for dialogue and cooperation. Heritage tourism offers economic opportunities for locals, supporting sustainable development through authentic experiences.

    How can we unleash the transformative power of heritage by fostering critical awareness across NSW and getting people to cherish their local heritage items?


2. Brisbane City Experience
   https://hackerspace.govhack.org/challenges/brisbane_city_experience


    As Australia’s largest council by number of residents, with ample data available, here is a challenge to connect, analyse, and utilise open data to enhance the Brisbane City experience for citizens, businesses, and visitors.

    How can you use open data from various portals to enhance the experience for (any combination of) residents, visitors, and businesses? Be creative! Mash up data across different topics. Winning this challenge will require creativity, ingenuity, and an exploration of the diverse range of datasets available.

    Try not to duplicate any efforts already contained within the Council owned and run Brisbane App.


3. Tagging Photographic Images (?)
   https://hackerspace.govhack.org/challenges/tagging_photographic_images_showcasing_the_magnificent_history_of_victoria


    There are thousands of photos in the collections and manually adding metadata about each photo would take a mammoth effort.

    Can you find a way to use new technologies and tools, crowdsource, or other way to tag images so that they can be used on the PROV website Apache Solr instance for enhanced search?

    Technical information about enhanced search can be found on the GitHub project.

    Test UI available at this link - https://prov.conaltuohy.com/iiif-collection-photo-wall/iiif-collection-photo-wall.html (username and password can be found in the #talk-publicrecordsvic Slack channel)


4. Remix the archives using PROV API
   https://hackerspace.govhack.org/challenges/remix_the_archives_using_the_prov_api

    What do we mean by remix?

    Here is an excerpt from Remix Theory that explains one definition of remix:

    “Generally speaking, remix culture can be defined as the global activity consisting of the creative and efficient exchange of information made possible by digital technologies that is supported by the practice of cut/copy and paste.

    The concept of Remix often referenced in popular culture derives from the model of music remixes which were produced around the late 1960s and early 1970s in New York City, an activity with roots in Jamaica’s music.

    Today, Remix (the activity of taking samples from pre-existing materials to combine them into new forms according to personal taste) has been extended to other areas of culture, including the visual arts; it plays a vital role in mass communication, especially on the Internet”.

    See also State Library Victoria archives: https://www.slv.vic.gov.au/images?keyword=flinders%20st&smt=1

5. Generative AI : unleashing the power of open data
   https://hackerspace.govhack.org/challenges/generative_ai_unleashing_the_power_of_open_data

   Project Considerations:
   1. Ethical Use of Generative AI: Participants should prioritise ethical considerations while leveraging Generative AI models. Solutions that promote inclusivity, transparency, and accountability will be highly valued.
   2. Data Privacy and Protection: Respect the privacy and confidentiality of individuals and sensitive information present in the Open Data. Implement appropriate measures to ensure data protection and anonymity where applicable.
   3. Copyright and Attribution: Adhere to copyright laws and respect intellectual property rights when using external data sources. Proper referencing and attribution of datasets and AI tools are mandatory.
   4. Community Benefit: Emphasise the potential positive impact of the proposed solutions on communities. Projects that address real-world challenges and contribute to community welfare will be recognised.

   Submission Guidelines:

   * Participants should demonstrate a comprehensive understanding of Generative AI and its applications in analysing Open Data.
   * Properly reference the Generative AI models and tools used, along with the sources of Open Data employed.
   * All projects should be the original work of the participants, showcasing thoughtful and innovative solutions.