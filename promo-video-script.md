# Script

Solo Traveller is an app that was built by "Team Solo", a group of people who came to this event without any plans or expectations, and who only met 46 hours before the project was completed.

As a team we are convinced of the power of local stories and folklore to provide a sense of connection to community, something that seems to have deteriorated of late. Social networks promise connectedness, yet we have a loneliness epidemic, and society is becoming more insular.  People are increasingly only connected online through niche interest groups, and as a consequence are disconnected from their neighbours and their real-life communities.

COVID19, and the subsequent shift to work-from-home that it has driven, has led to further disconnection from work colleagues, and only amplified the sense of isolation for many.

Solo Traveller is an opportunity to do something about this.  To bring back a sense of local community, an understanding and a proudness of one's place in the world, and to provide a sense that one can also play a part in a much bigger whole.

So join us in our app, Solo Traveller, and discover a multitude of curated stories, multimedia sounds, video and imagery relating to landmarks near you.  

Our stories are sourced from diverse Australia-wide datasets, including the Public Records Office of Victoria, the New South Wales State Archives Collection, the State Library of Victoria, the Australian Tourism Data Warehouse, Brisbane Community Profiles and Public Art collection, as well as allowing local guides the ability to supply otherwise unrecorded community-based information.

Solo Traveller allows you to travel and explore locally, or around Australia, while discovering a newfound sense of connection with this beautiful land and it's amazing people.

We use React and Typescript for the front-end of our codebase, and Eleven Labs generative AI for text-to-speech.

Our stories are geocoded, and we use leaflet.js for mapping.

If you'd like to try solo traveller yourself, a link to the system can be found below.  Source code is also available on gitlab if you'd like to have a look, or even better, contribute yourself.

Thank you for your time!  We've enjoyed working together as a team and hope you appreciate our work too.
